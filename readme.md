# Torbor Elliptics

## Установка

### Composer

	php composer.phar require torbor/elliptics "dev-master"

или

	"torbor/elliptics": "dev-master"

Так же необходимо добавить в composer.json

    "repositories": [
        {
            "type": "git",
            "url":  "git@bitbucket.org:torbor_team/elliptics.git"
        }
    ],
    

## Использование

    $elliptics = new \Torbor\elliptics\Elliptics($url, $prefix);
    
Подключение в yii2 в service locator:

    'elliptics' => function() {
        return new \Torbor\elliptics\Elliptics($url, $prefix);
    }

Получение пути к файлу: 
   
    $elliptics->get($filename);
    
Загрузка файла: 
    
    $elliptics->upload($filename, $pathToFile);
    
Удаление файла: 
    
    $elliptics->delete($filename);
    
Удаление нескольких файлов: 
    
    $elliptics->bulkDelete(['file1', 'file2', 'file3'], $bucket);

где $bucket это имя нэймспейса для файлов