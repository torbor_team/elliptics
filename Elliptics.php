<?php

namespace Torbor\elliptics;

use Torbor\elliptics\Exception\FileNotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

/**
 * Класс для работы с файлами в Elliptics
 *
 * Получение пути к файлу: $elliptics->get('file.name');
 * Загрузка файла: $elliptics->upload($filename, $pathToFile);
 * Удаление файла: $elliptics->delete($filename);
 * Удаление нескольких файлов: $elliptics->bulkDelete(['file1', 'file2', 'file3'], 'torbor');
 */
class Elliptics
{
    /**
     * @var string Url к серверу Elliptics
     */
    private $url;

    /**
     * @var string Prefix для Elliptics
     */
    private $prefix;

    /**
     * Elliptics constructor.
     *
     * @param string $url
     * @param string $prefix
     */
    public function __construct(string $url, string $prefix)
    {
        $this->url = $url;
        $this->prefix = $prefix;
    }

    /**
     * Получение ссылки к файлу filename
     *
     * @param string $filename Имя файла
     * @return string
     */
    public function get($filename): string
    {
        return $this->getPath([$this->url, $this->prefix, $filename]);
    }

    /**
     * Загрузка файла в Elliptics
     *
     * @param string $filename Имя файла
     * @param string $pathToFile Путь к файлу
     * @return mixed
     */
    public function upload(string $filename, string $pathToFile)
    {
        $response = (new Client())->post($this->getPath([$this->url, 'upload', $this->prefix, $filename]), [
            'body' => fopen($pathToFile, 'rb')
        ]);

        return $this->jsonDecode($response);
    }

    /**
     * Удаление файла
     *
     * @param string $filename Имя удаляемого файла
     * @return mixed
     * @throws FileNotFoundException
     */
    public function delete(string $filename)
    {
        try {
            $response = (new Client())->post($this->getPath([$this->url, 'delete', $this->prefix, $filename]));
            return $this->jsonDecode($response);
        } catch (ClientException $e) {
            throw new FileNotFoundException($e->getMessage());
        }
    }

    /**
     * Удаление файлов
     *
     * @param array $files Список имен удаляемых файлов
     * @param string $bucket - нэймспейс откуда удаляются файлы
     * @return mixed
     */
    public function bulkDelete(array $files, $bucket)
    {
        $filesWithPrefix = array_map(function ($file) {
            return $this->getPath([$this->prefix, $file]);
        }, $files);

        $response = (new Client())->post($this->getPath([$this->url, 'bulk_delete', $bucket]), [
            'body' => \GuzzleHttp\json_encode(['keys' => $filesWithPrefix])
        ]);

        return $this->jsonDecode($response);
    }

    /**
     * Возвращает Json Decode от тела ответа
     *
     * @param ResponseInterface $response
     * @return mixed
     */
    private function jsonDecode(ResponseInterface $response)
    {
        if (!(string)$response->getBody()) {
            return [];
        }

        return \GuzzleHttp\json_decode($response->getBody());
    }

    /**
     * Получить путь с разделителем из его частей
     *
     * @param array $parts Массив содержащий части пути
     * @return string
     */
    private function getPath(array $parts): string
    {
        return implode('/', $parts);
    }
}