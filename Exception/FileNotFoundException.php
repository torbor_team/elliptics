<?php

namespace Torbor\elliptics\Exception;

/**
 * Exception file not found
 */
class FileNotFoundException extends \Exception {}
